package com.myproject.myworkshop.repository;


import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstituteWhoRelatedToWorkShopRepository extends CrudRepository<InstituteWhoRelatedToWorkShopRelation,Integer> {
 }
