package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ParticipantGroupRelationRepository extends CrudRepository<ParticipantGroupRelation,Integer> {
   }
