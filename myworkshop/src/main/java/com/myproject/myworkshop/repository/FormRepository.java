package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.form.Form;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormRepository extends CrudRepository<Form,Integer> {


    Form findByName(String name);
}
