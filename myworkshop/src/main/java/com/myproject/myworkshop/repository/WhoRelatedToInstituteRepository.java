package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WhoRelatedToInstituteRepository extends CrudRepository<WhoRelatedToInstitute,Integer> {
    }
