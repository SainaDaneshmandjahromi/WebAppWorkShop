package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.model.institute.WorkShopWorkShopRelation;
import org.springframework.data.repository.CrudRepository;


import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository

public interface WorkShopWorkShopRelationRepository extends CrudRepository<WorkShopWorkShopRelation, Integer> {

}

