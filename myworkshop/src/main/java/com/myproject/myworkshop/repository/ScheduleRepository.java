package com.myproject.myworkshop.repository;


import com.myproject.myworkshop.model.institute.Schedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule,Integer> {
    Iterable<Schedule> findAll();

}
