package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferedWorkShopRepository extends CrudRepository<OfferedWorkShop, Integer> {

    Iterable<OfferedWorkShop> findAll();
}
