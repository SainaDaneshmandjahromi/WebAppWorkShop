package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.institute.Institute;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstituteRepository extends CrudRepository<Institute,Integer> {

    Institute findByName(String name);
}
