package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.user.workshoprelated.Organizer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizerRepository extends CrudRepository<Organizer, Integer> {
}
