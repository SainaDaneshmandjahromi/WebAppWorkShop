package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.institute.WorkShop;
import org.springframework.data.repository.CrudRepository;


import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository

public interface WorkShopRepository extends CrudRepository<WorkShop, Integer> {


    WorkShop findByName(String name);
}

