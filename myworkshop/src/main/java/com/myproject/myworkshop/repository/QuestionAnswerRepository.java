package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionAnswerRepository extends CrudRepository<QuestionAnswer,Integer> {

//    void update(QuestionAnswer questionAnswer);
}
