package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.user.myUser;
import org.springframework.data.repository.CrudRepository;


import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository

public interface UserRepository extends CrudRepository<myUser, Integer> {
   // myUser findByName(String name);
    myUser findByUserName(String username);
    Optional<myUser> findById(Integer id);

}

