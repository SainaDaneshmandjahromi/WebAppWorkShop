package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface myGroupRepository extends CrudRepository<myGroup,Integer> {

    myGroup findByName(String username);
}
