package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizerWorkShopRelationRepository extends CrudRepository<OrganizerWorkshopRelation,Integer> {
   }
