package com.myproject.myworkshop.repository;


import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.GraderGroupRelation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GraderGroupRelationRepository extends CrudRepository<GraderGroupRelation, Integer> {

}
