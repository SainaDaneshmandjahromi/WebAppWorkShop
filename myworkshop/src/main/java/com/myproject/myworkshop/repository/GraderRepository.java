package com.myproject.myworkshop.repository;

import com.myproject.myworkshop.model.user.workshoprelated.Grader;
import com.myproject.myworkshop.model.user.workshoprelated.Organizer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GraderRepository extends CrudRepository<Grader, Integer> {
}
