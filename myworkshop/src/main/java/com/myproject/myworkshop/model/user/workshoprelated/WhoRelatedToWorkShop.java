package com.myproject.myworkshop.model.user.workshoprelated;


import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import com.myproject.myworkshop.model.institute.WorkShopWorkShopRelation;
import com.myproject.myworkshop.model.user.Role;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class WhoRelatedToWorkShop extends Role {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date strartdate;
	
	@Temporal(TemporalType.DATE)
	private Date enddate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStrartdate() {
		return strartdate;
	}

	public void setStrartdate(Date strartdate) {
		this.strartdate = strartdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
}
	
	

