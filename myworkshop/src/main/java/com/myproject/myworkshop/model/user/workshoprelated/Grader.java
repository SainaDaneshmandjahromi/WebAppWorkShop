package com.myproject.myworkshop.model.user.workshoprelated;


import com.myproject.myworkshop.model.institute.OfferedWorkShop;

import javax.persistence.*;
import java.security.acl.Group;
import java.util.List;

@Entity
public class Grader extends WhoRelatedToWorkShop {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Override
	public Integer getId() {
		return id;
	}

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn
    private myGroup mygroup;


    @OneToMany(mappedBy="grader")
	private List<GraderGroupRelation> graderGroupRelation;

    public myGroup getMygroup() {
        return mygroup;
    }

    public void setMygroup(myGroup mygroup) {
        this.mygroup = mygroup;
    }

    public List<GraderGroupRelation> getGraderGroupRelation() {
        return graderGroupRelation;
    }

    public void setGraderGroupRelation(List<GraderGroupRelation> graderGroupRelation) {
        this.graderGroupRelation = graderGroupRelation;
    }
}




