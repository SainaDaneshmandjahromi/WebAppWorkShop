package com.myproject.myworkshop.model.alaki;

import java.util.ArrayList;
import java.util.List;

public class ForFillingForm {

    private String Name;
    private String Answer;
    private int whichQuestion;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public int getWhichQuestion() {
        return whichQuestion;
    }

    public void setWhichQuestion(int whichQuestion) {
        this.whichQuestion = whichQuestion;
    }
}
