package com.myproject.myworkshop.model.institute;

import com.myproject.myworkshop.model.institute.InstitueType;
import com.myproject.myworkshop.model.user.ContactPoint;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class Institute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	InstitueType institueType;


	private String name;
	private String description;


	@OneToMany(mappedBy = "institute")
	private List<Institute> institutes;

	@ManyToOne
	@JoinColumn
	private Institute institute;


	@OneToMany(mappedBy = "institute")
	private List<News> news;


	@OneToMany(mappedBy = "institute")
	private List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelation;

	@OneToMany(mappedBy = "institute")
	private List<ContactPoint> contactPoints;


	@OneToMany(mappedBy = "institute")
	private List<WorkShop> workShops;

	public Integer getId() {
		return id;
	}


	public InstitueType getInstitueType() {
		return institueType;
	}

	public void setInstitueType(InstitueType institueType) {
		this.institueType = institueType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Institute> getInstitutes() {
		return institutes;
	}

	public void setInstitutes(List<Institute> institutes) {
		this.institutes = institutes;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public List<News> getNews() {
		return news;
	}

	public void setNews(List<News> news) {
		this.news = news;
	}

	public List<InstituteWhoRelatedToWorkShopRelation> getInstituteWhoRelatedToWorkShopRelation() {
		return instituteWhoRelatedToWorkShopRelation;
	}

	public void setInstituteWhoRelatedToWorkShopRelation(List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelation) {
		this.instituteWhoRelatedToWorkShopRelation = instituteWhoRelatedToWorkShopRelation;
	}

	public List<ContactPoint> getContactPoints() {
		return contactPoints;
	}

	public void setContactPoints(List<ContactPoint> contactPoints) {
		this.contactPoints = contactPoints;
	}

	public List<WorkShop> getWorkShops() {
		return workShops;
	}

	public void setWorkShops(List<WorkShop> workShops) {
		this.workShops = workShops;
	}
}
