package com.myproject.myworkshop.model.institute;

import com.myproject.myworkshop.model.institute.SubjectOfWorkShop;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class WorkShop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String name;

	private String description;

	@Enumerated(EnumType.STRING)
	private SubjectOfWorkShop subjectOfWorkShop;

	@OneToMany(mappedBy = "workShop")
	private List<OfferedWorkShop> offeredWorkShops;


	@OneToMany(mappedBy = "workshop")
	private List<WorkShopWorkShopRelation> workShopWorkShopRelation;


	@ManyToOne
	@JoinColumn
	private Institute institute;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SubjectOfWorkShop getSubjectOfWorkShop() {
		return subjectOfWorkShop;
	}

	public void setSubjectOfWorkShop(SubjectOfWorkShop subjectOfWorkShop) {
		this.subjectOfWorkShop = subjectOfWorkShop;
	}

	public List<OfferedWorkShop> getOfferedWorkShops() {
		return offeredWorkShops;
	}

	public void setOfferedWorkShops(List<OfferedWorkShop> offeredWorkShops) {
		this.offeredWorkShops = offeredWorkShops;
	}

	public List<WorkShopWorkShopRelation> getWorkShopWorkShopRelation() {
		return workShopWorkShopRelation;
	}

	public void setWorkShopWorkShopRelation(List<WorkShopWorkShopRelation> workShopWorkShopRelation) {
		this.workShopWorkShopRelation = workShopWorkShopRelation;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}
}


