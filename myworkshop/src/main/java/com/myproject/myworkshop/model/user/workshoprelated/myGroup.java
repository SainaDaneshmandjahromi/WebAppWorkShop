package com.myproject.myworkshop.model.user.workshoprelated;


import com.myproject.myworkshop.model.form.Formable;
import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;

import javax.persistence.*;
import java.util.List;

@Entity
public class myGroup extends Formable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //@Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private boolean isBranch;

    private String name;

    @OneToMany(mappedBy="mygroup")
    private List<ParticipantGroupRelation> participantGroupRelations;

    @OneToMany(mappedBy="mygroup")
    private List<GraderGroupRelation> graderGroupRelation;


    @OneToMany(mappedBy="mygroup")
    private List<myGroup> myGroups;

    @OneToMany(mappedBy="mygroup")
    private List<Grader> graders;


    @OneToMany(mappedBy="mygroup")
    private List<Participant> participants;


    @ManyToOne
    @JoinColumn
    private myGroup mygroup;

    @ManyToOne
    @JoinColumn
    private OfferedWorkShop offeredWorkShop;

    public List<ParticipantGroupRelation> getParticipantGroupRelations() {
        return participantGroupRelations;
    }

    public void setParticipantGroupRelations(List<ParticipantGroupRelation> participantGroupRelations) {
        this.participantGroupRelations = participantGroupRelations;
    }

    public List<GraderGroupRelation> getGraderGroupRelation() {
        return graderGroupRelation;
    }

    public void setGraderGroupRelation(List<GraderGroupRelation> graderGroupRelation) {
        this.graderGroupRelation = graderGroupRelation;
    }

    public List<myGroup> getMyGroups() {
        return myGroups;
    }

    public void setMyGroups(List<myGroup> myGroups) {
        this.myGroups = myGroups;
    }

    public myGroup getMygroup() {
        return mygroup;
    }

    public void setMygroup(myGroup mygroup) {
        this.mygroup = mygroup;
    }

    public OfferedWorkShop getOfferedWorkShop() {
        return offeredWorkShop;
    }

    public void setOfferedWorkShop(OfferedWorkShop offeredWorkShop) {
        this.offeredWorkShop = offeredWorkShop;
    }

    public boolean isBranch() {
        return isBranch;
    }

    public void setBranch(boolean branch) {
        isBranch = branch;
    }

    public List<Grader> getGraders() {
        return graders;
    }

    public void setGraders(List<Grader> graders) {
        this.graders = graders;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}




