package com.myproject.myworkshop.model.user.workshoprelated;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class Payment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;


    @Temporal(TemporalType.DATE)
    private Date enddate;

    boolean isPayed;

    @ManyToOne
    @JoinColumn
    private ParticipantGroupRelation participantGroupRelation;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}



