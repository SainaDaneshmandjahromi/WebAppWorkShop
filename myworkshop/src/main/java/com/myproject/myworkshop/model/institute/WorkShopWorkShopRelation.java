package com.myproject.myworkshop.model.institute;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class WorkShopWorkShopRelation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
    WorkShopWorkShopRelationType workShopWorkShopRelationType;


	@ManyToOne
	@JoinColumn
	private WorkShop workshop;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}





	}




