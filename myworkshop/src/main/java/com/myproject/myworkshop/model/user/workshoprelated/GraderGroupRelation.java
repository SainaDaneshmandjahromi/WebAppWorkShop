package com.myproject.myworkshop.model.user.workshoprelated;



import com.myproject.myworkshop.model.form.Formable;

import javax.persistence.*;

@Entity
public class GraderGroupRelation extends Formable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Boolean IsArshad;

	@Enumerated(EnumType.STRING)
	private IsAccepted isAccepted;


	@ManyToOne
	@JoinColumn
	private Grader grader;

	@ManyToOne
	@JoinColumn
	private myGroup mygroup;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getArshad() {
		return IsArshad;
	}

	public void setArshad(Boolean arshad) {
		IsArshad = arshad;
	}

	public IsAccepted getIsAccepted() {
		return isAccepted;
	}

	public void setIsAccepted(IsAccepted isAccepted) {
		this.isAccepted = isAccepted;
	}

	public Grader getGrader() {
		return grader;
	}

	public void setGrader(Grader grader) {
		this.grader = grader;
	}

	public myGroup getMygroup() {
		return mygroup;
	}

	public void setMygroup(myGroup mygroup) {
		this.mygroup = mygroup;
	}
}



