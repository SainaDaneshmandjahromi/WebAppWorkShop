package com.myproject.myworkshop.model.form;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Form implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

	@OneToMany(mappedBy="form")
	private List<FormData> formData;

	@OneToMany(mappedBy="form")
	private List<FormFormableRelation> formFormableRelation;

    @ManyToOne
    @JoinColumn
    private OfferedWorkShop offeredWorkShop;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FormData> getFormData() {
        return formData;
    }

    public void setFormData(List<FormData> formData) {
        this.formData = formData;
    }

    public List<FormFormableRelation> getFormFormableRelation() {
        return formFormableRelation;
    }

    public void setFormFormableRelation(List<FormFormableRelation> formFormableRelation) {
        this.formFormableRelation = formFormableRelation;
    }

    public OfferedWorkShop getOfferedWorkShop() {
        return offeredWorkShop;
    }

    public void setOfferedWorkShop(OfferedWorkShop offeredWorkShop) {
        this.offeredWorkShop = offeredWorkShop;
    }

}





