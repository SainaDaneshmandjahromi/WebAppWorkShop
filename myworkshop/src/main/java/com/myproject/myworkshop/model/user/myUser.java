package com.myproject.myworkshop.model.user;

import com.myproject.myworkshop.model.user.Gender;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
//@Getter
public class myUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String userName;
    private String password;


    @Enumerated(EnumType.STRING)
    private Gender gender;

	@OneToMany(mappedBy="user", fetch = FetchType.EAGER)
	private List<Role> roles;

	@OneToMany(mappedBy="myuser")
	private List<ContactPoint> contactPoints;

	boolean equals(myUser anotherUser){
	    return this.getUserName().equals(anotherUser.getUserName());
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<ContactPoint> getContactPoints() {
        return contactPoints;
    }

    public void setContactPoints(List<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }
}


