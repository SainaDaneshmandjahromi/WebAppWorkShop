package com.myproject.myworkshop.model.institute;

import com.myproject.myworkshop.model.form.Form;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class OfferedWorkShop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer cost;

	@Temporal(TemporalType.DATE)
	private Date startdate;

	@Temporal(TemporalType.DATE)
	private Date enddate;

	@OneToMany(mappedBy="offeredworkshop")
	private List<Schedule> schedule;

	@OneToMany(mappedBy="offeredWorkShop")
	private List<Form> form;

	@OneToMany(mappedBy="offeredWorkShop")
	private List<myGroup> mygroup;

	@OneToMany(mappedBy="offeredworkshop")
	private List<OrganizerWorkshopRelation> organizerWorkshopRelation;

	@ManyToOne
	@JoinColumn
	private WorkShop workShop;

	public Integer getId() {
		return id;
	}

	public WorkShop getWorkShop() {
		return workShop;
	}

	public void setWorkShop(WorkShop workShop) {
		this.workShop = workShop;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public List<Schedule> getSchedule() {
		return schedule;
	}

	public void setSchedule(List<Schedule> schedule) {
		this.schedule = schedule;
	}

	public List<Form> getForm() {
		return form;
	}

	public void setForm(List<Form> form) {
		this.form = form;
	}

	public List<myGroup> getMygroup() {
		return mygroup;
	}

	public void setMygroup(List<myGroup> mygroup) {
		this.mygroup = mygroup;
	}

	public List<OrganizerWorkshopRelation> getOrganizerWorkshopRelation() {
		return organizerWorkshopRelation;
	}

	public void setOrganizerWorkshopRelation(List<OrganizerWorkshopRelation> organizerWorkshopRelation) {
		this.organizerWorkshopRelation = organizerWorkshopRelation;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}
}



