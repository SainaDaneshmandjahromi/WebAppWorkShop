package com.myproject.myworkshop.model.user.workshoprelated;

import com.myproject.myworkshop.model.form.Formable;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;

import javax.persistence.*;

@Entity
public class OrganizerWorkshopRelation extends Formable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn
	private OfferedWorkShop offeredWorkshop;

	@ManyToOne
	@JoinColumn
	private Organizer organizer;

	@ManyToOne
	@JoinColumn
	private OfferedWorkShop offeredworkshop;


	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public OfferedWorkShop getOfferedWorkshop() {
		return offeredWorkshop;
	}

	public void setOfferedWorkshop(OfferedWorkShop offeredWorkshop) {
		this.offeredWorkshop = offeredWorkshop;
	}

	public Organizer getOrganizer() {
		return organizer;
	}

	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	public OfferedWorkShop getOfferedworkshop() {
		return offeredworkshop;
	}

	public void setOfferedworkshop(OfferedWorkShop offeredworkshop) {
		this.offeredworkshop = offeredworkshop;
	}
}



