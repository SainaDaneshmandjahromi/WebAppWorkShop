package com.myproject.myworkshop.model.user;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import static com.myproject.myworkshop.security.SecurityConstants.SECRET_KEY;
import static com.myproject.myworkshop.security.SecurityConstants.TOKEN_PREFIX;

public class Token{
        private String token;

        public Token(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Claims decodeJWT() {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY.getBytes())
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody();

            return claims;
        }
}
