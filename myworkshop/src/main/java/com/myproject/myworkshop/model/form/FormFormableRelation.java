package com.myproject.myworkshop.model.form;

import com.myproject.myworkshop.model.user.workshoprelated.myGroup;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class FormFormableRelation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;


	@ManyToOne
	@JoinColumn
	private Formable formable;

	@ManyToOne
	@JoinColumn
	private Form form;

	@Enumerated(EnumType.STRING)
	private FormFormableRelationType formFormableRelationType;


	@Temporal(TemporalType.DATE)
	private Date startrel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Formable getFormable() {
		return formable;
	}

	public void setFormable(Formable formable) {
		this.formable = formable;
	}

	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public FormFormableRelationType getFormFormableRelationType() {
		return formFormableRelationType;
	}

	public void setFormFormableRelationType(FormFormableRelationType formFormableRelationType) {
		this.formFormableRelationType = formFormableRelationType;
	}

	public Date getStartrel() {
		return startrel;
	}

	public void setStartrel(Date startrel) {
		this.startrel = startrel;
	}

}



