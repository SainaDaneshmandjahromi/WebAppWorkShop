package com.myproject.myworkshop.model.user.workshoprelated;

import javax.persistence.*;
import java.util.List;

@Entity
public class Organizer extends WhoRelatedToWorkShop {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@OneToMany(mappedBy="organizer")
	private List<OrganizerWorkshopRelation> organizerWorkShopRelation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<OrganizerWorkshopRelation> getOrganizerWorkShopRelation() {
		return organizerWorkShopRelation;
	}

	public void setOrganizerWorkShopRelation(List<OrganizerWorkshopRelation> organizerWorkShopRelation) {
		this.organizerWorkShopRelation = organizerWorkShopRelation;
	}
}



