package com.myproject.myworkshop.model.user.workshoprelated;


import javax.persistence.*;
import java.util.List;

@Entity
public class Participant extends WhoRelatedToWorkShop {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@OneToMany(mappedBy="participant")
	private List<ParticipantGroupRelation> participantGroupRelation;

	@ManyToOne
	@JoinColumn
	private myGroup mygroup;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<ParticipantGroupRelation> getParticipantGroupRelation() {
		return participantGroupRelation;
	}

	public void setParticipantGroupRelation(List<ParticipantGroupRelation> participantGroupRelation) {
		this.participantGroupRelation = participantGroupRelation;
	}

	public myGroup getMygroup() {
		return mygroup;
	}

	public void setMygroup(myGroup mygroup) {
		this.mygroup = mygroup;
	}
}



