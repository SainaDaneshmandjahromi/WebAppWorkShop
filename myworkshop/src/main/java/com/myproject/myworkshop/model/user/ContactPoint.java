package com.myproject.myworkshop.model.user;

import com.myproject.myworkshop.model.institute.Institute;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class ContactPoint implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;


	private String ContactData;

	private String Comment;

	@Enumerated(EnumType.STRING)
	private ContactPointType contactPointType;

	@ManyToOne
	@JoinColumn
	private Role role;

	@ManyToOne
	@JoinColumn
	private Institute institute;

	@ManyToOne
	@JoinColumn
	private myUser myuser;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContactData() {
		return ContactData;
	}

	public void setContactData(String contactData) {
		ContactData = contactData;
	}

	public ContactPointType getContactPointType() {
		return contactPointType;
	}

	public void setContactPointType(ContactPointType contactPointType) {
		this.contactPointType = contactPointType;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public myUser getUser() {
		return myuser;
	}

	public void setUser(myUser myuser) {
		this.myuser = myuser;
	}






}
