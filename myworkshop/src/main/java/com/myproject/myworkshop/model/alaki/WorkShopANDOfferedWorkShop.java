package com.myproject.myworkshop.model.alaki;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.Grader;
import com.myproject.myworkshop.model.user.workshoprelated.Organizer;

public class WorkShopANDOfferedWorkShop {

    private WorkShop workShop;
    private OfferedWorkShop offeredWorkShop;
    private myUser organizer;

    public WorkShop getWorkShop() {
        return workShop;
    }

    public void setWorkShop(WorkShop workShop) {
        this.workShop = workShop;
    }

    public OfferedWorkShop getOfferedWorkShop() {
        return offeredWorkShop;
    }

    public void setOfferedWorkShop(OfferedWorkShop offeredWorkShop) {
        this.offeredWorkShop = offeredWorkShop;
    }

    public myUser getOrganizer() {
        return organizer;
    }

    public void setOrganizer(myUser organizer) {
        this.organizer = organizer;
    }

}


