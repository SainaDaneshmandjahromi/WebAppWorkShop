package com.myproject.myworkshop.model.user;

import com.myproject.myworkshop.model.user.Gender;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String NationalCode;

	@Temporal(TemporalType.DATE)
	private Date birthdate;

	@OneToMany(mappedBy="role")
	private List<ContactPoint> contactPoints;

	@ManyToOne
	@JoinColumn
	private myUser user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNationalCode() {
		return NationalCode;
	}

	public void setNationalCode(String nationalCode) {
		NationalCode = nationalCode;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public List<ContactPoint> getContactPoints() {
		return contactPoints;
	}

	public void setContactPoints(List<ContactPoint> contactPoints) {
		this.contactPoints = contactPoints;
	}

	public myUser getUser() {
		return user;
	}

	public void setUser(myUser user) {
		this.user = user;
	}
}



