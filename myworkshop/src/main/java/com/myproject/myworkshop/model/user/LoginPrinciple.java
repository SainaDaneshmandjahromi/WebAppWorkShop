package com.myproject.myworkshop.model.user;

import java.util.HashMap;
import java.util.Map;

public class LoginPrinciple {
    private String id;

    public LoginPrinciple(Object principal) {
        Map map = (HashMap) principal;
        id = (String) map.get("id");
    }

    public LoginPrinciple(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

