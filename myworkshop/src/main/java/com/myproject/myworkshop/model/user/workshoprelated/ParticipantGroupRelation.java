package com.myproject.myworkshop.model.user.workshoprelated;

import com.myproject.myworkshop.model.form.Formable;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class ParticipantGroupRelation extends Formable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private int Grade;


	@OneToMany(mappedBy="participantGroupRelation")
	private List<Payment> payments;

	@ManyToOne
	@JoinColumn
	private myGroup mygroup;


	@ManyToOne
	@JoinColumn
	private Participant participant;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getGrade() {
		return Grade;
	}

	public void setGrade(int grade) {
		Grade = grade;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public myGroup getMygroup() {
		return mygroup;
	}

	public void setMygroup(myGroup mygroup) {
		this.mygroup = mygroup;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
}
	
	

