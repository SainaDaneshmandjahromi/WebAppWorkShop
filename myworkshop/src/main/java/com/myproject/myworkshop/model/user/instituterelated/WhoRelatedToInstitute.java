package com.myproject.myworkshop.model.user.instituterelated;



import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import com.myproject.myworkshop.model.user.Role;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class WhoRelatedToInstitute extends Role {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date strartdate;

	@Temporal(TemporalType.DATE)
	private Date enddate;


	@OneToMany(mappedBy="institute")
	private List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelation ;

	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStrartdate() {
		return strartdate;
	}

	public void setStrartdate(Date strartdate) {
		this.strartdate = strartdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public List<InstituteWhoRelatedToWorkShopRelation> getInstituteWhoRelatedToWorkShopRelation() {
		return instituteWhoRelatedToWorkShopRelation;
	}

	public void setInstituteWhoRelatedToWorkShopRelation(List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelation) {
		this.instituteWhoRelatedToWorkShopRelation = instituteWhoRelatedToWorkShopRelation;
	}
}



