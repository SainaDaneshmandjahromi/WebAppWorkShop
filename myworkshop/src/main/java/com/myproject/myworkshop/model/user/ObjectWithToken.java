package com.myproject.myworkshop.model.user;

public class ObjectWithToken {
    Object object;
    Token token;

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
