package com.myproject.myworkshop.model.institute;

import com.myproject.myworkshop.model.institute.Day;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	private Day day;
	
	@Temporal(TemporalType.DATE)
	private Date startdate;

	@Temporal(TemporalType.DATE)
	private Date enddate;

	@ManyToOne
	@JoinColumn
	private OfferedWorkShop offeredworkshop;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public OfferedWorkShop getOfferedworkshop() {
		return offeredworkshop;
	}

	public void setOfferedworkshop(OfferedWorkShop offeredworkshop) {
		this.offeredworkshop = offeredworkshop;
	}
}
	
	

