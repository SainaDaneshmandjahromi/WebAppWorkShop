package com.myproject.myworkshop.model.form;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Formable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;


	@OneToMany(mappedBy = "formable")
	private List<FormFormableRelation> formFormableRelation;

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public List<FormFormableRelation> getFormFormableRelation() {
		return formFormableRelation;
	}

	public void setFormFormableRelation(List<FormFormableRelation> formFormableRelation) {
		this.formFormableRelation = formFormableRelation;
	}
}



