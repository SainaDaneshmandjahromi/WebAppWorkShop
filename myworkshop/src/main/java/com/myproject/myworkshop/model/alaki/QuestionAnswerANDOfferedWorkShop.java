package com.myproject.myworkshop.model.alaki;

import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.user.myUser;
import org.apache.catalina.User;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class QuestionAnswerANDOfferedWorkShop {

    private List<String> questionAnswers;
    private OfferedWorkShop offeredWorkShop;
    private String Name;//esme form
    private List<myUser> myuser ;
    private AboutWhom aboutwhom;
    private String groupName;


    public List<String> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<String> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }

    public OfferedWorkShop getOfferedWorkShop() {
        return offeredWorkShop;
    }

    public void setOfferedWorkShop(OfferedWorkShop offeredWorkShop) {
        this.offeredWorkShop = offeredWorkShop;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<myUser> getMyuser() {
        return myuser;
    }

    public void setMyuser(List<myUser> myuser) {
        this.myuser = myuser;
    }

    public AboutWhom getAboutwhom() {
        return aboutwhom;
    }

    public void setAboutwhom(AboutWhom aboutwhom) {
        this.aboutwhom = aboutwhom;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
