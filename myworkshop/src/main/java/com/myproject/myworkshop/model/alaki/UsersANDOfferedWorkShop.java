package com.myproject.myworkshop.model.alaki;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.user.myUser;

import java.util.ArrayList;
import java.util.List;

public class UsersANDOfferedWorkShop {

    List<Object> myobjects = new ArrayList<>();
    OfferedWorkShop offeredWorkShop = new OfferedWorkShop();

    public List<Object> getMyobjects() {
        return myobjects;
    }

    public void setMyobjects(List<Object> myobjects) {
        this.myobjects = myobjects;
    }

    public OfferedWorkShop getOfferedWorkShop() {
        return offeredWorkShop;
    }

    public void setOfferedWorkShop(OfferedWorkShop offeredWorkShop) {
        this.offeredWorkShop = offeredWorkShop;
    }
}
