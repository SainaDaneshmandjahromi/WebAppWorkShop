package com.myproject.myworkshop.model.institute;



import com.myproject.myworkshop.model.user.instituterelated.AccountantOrManager;
import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;


import java.io.Serializable;

import javax.persistence.*;

@Entity
public class InstituteWhoRelatedToWorkShopRelation implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	AccountantOrManager accountantOrManager;

	@ManyToOne
	@JoinColumn
	private Institute institute;

	@ManyToOne
	@JoinColumn
	private WhoRelatedToInstitute whoRelatedToInstitute;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AccountantOrManager getAccountantOrManager() {
		return accountantOrManager;
	}

	public void setAccountantOrManager(AccountantOrManager accountantOrManager) {
		this.accountantOrManager = accountantOrManager;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public WhoRelatedToInstitute getWhoRelatedToInstitute() {
		return whoRelatedToInstitute;
	}

	public void setWhoRelatedToInstitute(WhoRelatedToInstitute whoRelatedToInstitute) {
		this.whoRelatedToInstitute = whoRelatedToInstitute;
	}
}




