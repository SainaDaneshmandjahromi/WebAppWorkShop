package com.myproject.myworkshop.controller;

import com.myproject.myworkshop.model.user.*;
import com.myproject.myworkshop.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping(value = "/register", produces = "application/json")/*In post bood*/
    public ResponseEntity register(@Valid @RequestBody myUser myuser, Errors errors) {
        if (errors.hasErrors()) {
            return new ResponseEntity(new myUser(), HttpStatus.BAD_REQUEST);
        }

        if (userService.findByUserName(myuser.getUserName()) != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new myUser());
        }

        myuser.setPassword(bCryptPasswordEncoder.encode(myuser.getPassword()));
        userService.save(myuser);

        return new ResponseEntity(myuser, HttpStatus.OK);
    }

    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity login(@RequestBody LoginUser loginUser) {
        if (userService.login(loginUser) != null)
            return new ResponseEntity(userService.login(loginUser), HttpStatus.OK);
        // mishe do khat ro hazf kard
        return new ResponseEntity(new myUser(), HttpStatus.NOT_FOUND);
    }

//
//    @GetMapping(value = "/login", produces = "application/json")
//    public ResponseEntity login() {
//        String hello = "Hello World";
//        System.out.println("hello");
//        return new ResponseEntity("succes", HttpStatus.OK);
//    }


//    @PostMapping
//    public ResponseEntity sayHello(){
//
//        return ResponseEntity<"Hello!",HttpStatus.OK>;
//    }
//////
////    @PostMapping(value = "/echo")
////    public String echo(@RequestBody String string){
////        return string;
////    }
}
