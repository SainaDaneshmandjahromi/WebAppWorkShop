package com.myproject.myworkshop.controller;

import com.myproject.myworkshop.model.alaki.AboutWhom;
import com.myproject.myworkshop.model.alaki.ForFillingForm;
import com.myproject.myworkshop.model.alaki.QuestionAnswerANDOfferedWorkShop;
import com.myproject.myworkshop.model.form.*;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.*;
import com.myproject.myworkshop.service.form.FormService;
import com.myproject.myworkshop.service.formformablerelation.FormFormableRelationService;
import com.myproject.myworkshop.service.group.myGroupService;
import com.myproject.myworkshop.service.questionanswer.QuestionAnswerService;
import com.myproject.myworkshop.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/form")
public class FormController {


    @Autowired
    private FormService formService;

    @Autowired
    private FormFormableRelationService formFormableRelationService;

    @Autowired
    private UserService userService;

    @Autowired
    private myGroupService mygroupService;

    @Autowired
    private QuestionAnswerService questionAnswerService;


    @PostMapping(value = "/create", produces = "application/json")
    public ResponseEntity createForm(@RequestHeader(value="Authorization") String token,@RequestBody QuestionAnswerANDOfferedWorkShop questionAnswerANDOfferedWorkShop) {
        Form form = new Form();
        List<FormData> formDatas= new ArrayList<>();
        List<Form> forms = new ArrayList<>();
        List<FormFormableRelation> formFormableRelations= new ArrayList<>();
        FormFormableRelation formFormableRelation = new FormFormableRelation();

        for(String string:questionAnswerANDOfferedWorkShop.getQuestionAnswers()){

            QuestionAnswer questionAnswer = new QuestionAnswer();
            questionAnswer.setQuestion(string);
            questionAnswerService.save(questionAnswer);
            formDatas.add(questionAnswer);
        }

        formService.save(form);
        formFormableRelation.setForm(form);
        formFormableRelation.setFormFormableRelationType(FormFormableRelationType.Designer);
        formFormableRelation.setFormable(questionAnswerANDOfferedWorkShop.getOfferedWorkShop().getOrganizerWorkshopRelation().get(0));


        FormFormableRelation formFormableRelationsec = new FormFormableRelation();
        formFormableRelationsec.setForm(form);
        formFormableRelationsec.setFormFormableRelationType(FormFormableRelationType.Filler);
        for(myGroup mygroup:questionAnswerANDOfferedWorkShop.getOfferedWorkShop().getMygroup()){
            for(GraderGroupRelation graderGroupRelation:mygroup.getGraderGroupRelation()){
                if(graderGroupRelation.getGrader().equals(questionAnswerANDOfferedWorkShop.getMyuser().get(0))){
                    formFormableRelationsec.setFormable( graderGroupRelation);
                }
            }
        }


        FormFormableRelation formFormableRelationthird = new FormFormableRelation();
        formFormableRelationthird.setForm(form);
        formFormableRelationthird.setFormFormableRelationType(FormFormableRelationType.DesignFor);
        if(questionAnswerANDOfferedWorkShop.getAboutwhom().equals(AboutWhom.Grader)){
            for(myGroup mygroup:questionAnswerANDOfferedWorkShop.getOfferedWorkShop().getMygroup()){
                for(GraderGroupRelation graderGroupRelation:mygroup.getGraderGroupRelation()){
                    if(graderGroupRelation.getGrader().equals(questionAnswerANDOfferedWorkShop.getMyuser().get(1))){
                        formFormableRelationsec.setFormable(graderGroupRelation);
                    }
                }
            }
        }
        else if(questionAnswerANDOfferedWorkShop.getAboutwhom().equals(AboutWhom.Participant)){
            for(myGroup mygroup:questionAnswerANDOfferedWorkShop.getOfferedWorkShop().getMygroup()){
                for(ParticipantGroupRelation participantGroupRelation:mygroup.getParticipantGroupRelations()){
                    if(participantGroupRelation.getParticipant().equals(questionAnswerANDOfferedWorkShop.getMyuser().get(1))){
                        formFormableRelationsec.setFormable(participantGroupRelation);

                        }
                }
            }
        }
        else if(questionAnswerANDOfferedWorkShop.getAboutwhom().equals(AboutWhom.Group)){
                myGroup mygroup = new myGroup();
                mygroup = mygroupService.findByName(questionAnswerANDOfferedWorkShop.getGroupName());
                formFormableRelationsec.setFormable(mygroup);

            }

        formFormableRelationService.save(formFormableRelation);
        formFormableRelations.add(formFormableRelation);
        formFormableRelationService.save(formFormableRelationsec);
        formFormableRelations.add(formFormableRelationsec);
        formFormableRelationService.save(formFormableRelationthird);
        formFormableRelations.add(formFormableRelationthird);

        form.setName(questionAnswerANDOfferedWorkShop.getName());
        form.setFormFormableRelation(formFormableRelations);
        form.setOfferedWorkShop(questionAnswerANDOfferedWorkShop.getOfferedWorkShop());
        forms = questionAnswerANDOfferedWorkShop.getOfferedWorkShop().getForm();
        forms.add(form);
        questionAnswerANDOfferedWorkShop.getOfferedWorkShop().setForm(forms);
        form.setFormData(formDatas);


        return new ResponseEntity(form,HttpStatus.OK);
    }

//    //todo token ro delete krdi bbin okeie
//    @PostMapping(value = "/fill", produces = "application/json")
//    public ResponseEntity fillForm(@RequestBody ForFillingForm forFillingForm) {
//                Form form = formService.findByName(forFillingForm.getName());
//                String answer = forFillingForm.getAnswer();
//                QuestionAnswer questionAnswer = new QuestionAnswer();
//                questionAnswer.getQuestion();
//                if(form.getFormData().get(forFillingForm.getWhichQuestion()) instanceof QuestionAnswer) {
//                    questionAnswer = (QuestionAnswer) form.getFormData().get(forFillingForm.getWhichQuestion());
//                    questionAnswer.setAnswer(answer);
//
//                }
//
//        return new ResponseEntity(questionAnswer,HttpStatus.OK);
//    }


    @PostMapping(value = "/show", produces = "application/json")
    public ResponseEntity showForm(@RequestHeader(value="Authorization") String token,@RequestBody OfferedWorkShop offeredWorkShop) {

        myUser myuser = userService.findUser(token);

        for(myGroup mygroup:offeredWorkShop.getMygroup()){
            for (GraderGroupRelation graderGroupRelation:mygroup.getGraderGroupRelation()){
                if(graderGroupRelation.getGrader().getUser().equals(myuser)){
                    for(FormFormableRelation formFormableRelation:graderGroupRelation.getFormFormableRelation()){
                        formFormableRelation.getFormFormableRelationType();
                        formFormableRelation.getForm().getName();
                    }
                }
            }
        }


        for(OrganizerWorkshopRelation organizerWorkshopRelation:offeredWorkShop.getOrganizerWorkshopRelation()){
            if(organizerWorkshopRelation.getOrganizer().getUser().equals(myuser)){
                for(FormFormableRelation formFormableRelation:organizerWorkshopRelation.getFormFormableRelation()){
                    formFormableRelation.getFormFormableRelationType();
                    formFormableRelation.getForm().getName();
                 }
            }
        }

        return new ResponseEntity(offeredWorkShop,HttpStatus.OK);
    }

    @PostMapping(value = "/fill", produces = "application/json")
    public ResponseEntity fillForm(@RequestHeader(value="Authorization") String token,@RequestBody ForFillingForm forFillingForm) {
        Form form = formService.findByName(forFillingForm.getName());
        String answer = forFillingForm.getAnswer();
        QuestionAnswer questionAnswer = new QuestionAnswer();
        questionAnswer.getQuestion();
        if(form.getFormData().get(forFillingForm.getWhichQuestion()) instanceof QuestionAnswer) {
            questionAnswer = (QuestionAnswer) form.getFormData().get(forFillingForm.getWhichQuestion());
            questionAnswer.setAnswer(answer);

        }
        questionAnswerService.save(questionAnswer);
       // questionAnswerService.update(form.getFormData().get(forFillingForm.getWhichQuestion()));

        return new ResponseEntity(questionAnswer,HttpStatus.OK);
    }

    @PostMapping(value = "/accept/grader", produces = "application/json")
    public ResponseEntity graderyacceptForm(@RequestHeader(value="Authorization") String token,@RequestBody ForFillingForm forFillingForm) {
        Form form = formService.findByName(forFillingForm.getName());
        String answer = forFillingForm.getAnswer();
        QuestionAnswer questionAnswer = new QuestionAnswer();
        questionAnswer.getQuestion();
        if(form.getFormData().get(forFillingForm.getWhichQuestion()) instanceof QuestionAnswer) {
            questionAnswer = (QuestionAnswer) form.getFormData().get(forFillingForm.getWhichQuestion());
            questionAnswer.setAnswer(answer);

        }
        questionAnswerService.save(questionAnswer);
        // questionAnswerService.update(form.getFormData().get(forFillingForm.getWhichQuestion()));

        return new ResponseEntity(questionAnswer,HttpStatus.OK);
    }





}
