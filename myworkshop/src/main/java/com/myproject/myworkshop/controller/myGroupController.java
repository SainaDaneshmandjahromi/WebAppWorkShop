package com.myproject.myworkshop.controller;

import com.myproject.myworkshop.model.alaki.UsersANDOfferedWorkShop;
import com.myproject.myworkshop.model.user.workshoprelated.*;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.Participant.ParticipantService;
import com.myproject.myworkshop.service.grader.GraderService;
import com.myproject.myworkshop.service.group.myGroupService;
import com.myproject.myworkshop.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/group")
public class myGroupController {

    @Autowired
    private InstituteService instituteService;

    @Autowired
    private UserService userService;


    @Autowired
    private myGroupService myGroupService;

    @Autowired
    private GraderService graderService;

    @Autowired
    private ParticipantService participantService;

    @PostMapping (value = "/create")
    //todo ye class besazm ba offeredworkshop va integer ha
    public ResponseEntity createGroup(@RequestHeader(value="Authorization") String token, @RequestBody UsersANDOfferedWorkShop usersANDOfferedWorkShop) {

        myGroup mygroup = new myGroup();
        List<myGroup> mygroups = new ArrayList<>();
        List<Grader> graders = new ArrayList<>();
        List<Participant> participants = new ArrayList<>();
        if(userService.isOrganizer(token,usersANDOfferedWorkShop.getOfferedWorkShop())){
            mygroups = usersANDOfferedWorkShop.getOfferedWorkShop().getMygroup();
            mygroup.setBranch(true);
            for(Object myobject:usersANDOfferedWorkShop.getMyobjects()){
                if(myobject instanceof Grader){
                    graderService.save((Grader) myobject);
                    graders.add((Grader) myobject);
                 }
                else{
                    participantService.save((Participant) myobject);
                    participants.add((Participant) myobject);
                }
            }
            mygroup.setGraders(graders);
            mygroup.setParticipants(participants);
            myGroupService.save(mygroup);
            mygroups = usersANDOfferedWorkShop.getOfferedWorkShop().getMygroup();
            mygroups.add(mygroup);
            usersANDOfferedWorkShop.getOfferedWorkShop().setMygroup(mygroups);

        }

        return new ResponseEntity(mygroup, HttpStatus.OK);
    }

}
