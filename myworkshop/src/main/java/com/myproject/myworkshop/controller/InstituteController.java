package com.myproject.myworkshop.controller;

import com.myproject.myworkshop.model.institute.*;
import com.myproject.myworkshop.model.user.Gender;
import com.myproject.myworkshop.model.user.Role;
import com.myproject.myworkshop.model.user.instituterelated.AccountantOrManager;
import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.*;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.gradergrouprelation.GraderGroupRelationService;
import com.myproject.myworkshop.service.group.myGroupService;
import com.myproject.myworkshop.service.institutewhorelatedtoworkshop.InstituteWhoRelatedToWorkShopService;
import com.myproject.myworkshop.service.myworkshop.WorkShopService;
import com.myproject.myworkshop.service.organizerworkshoprelation.OrganizerWorkShopRelationService;
import com.myproject.myworkshop.service.role.RoleService;
import com.myproject.myworkshop.service.user.UserService;
import com.myproject.myworkshop.service.whorelatedtoinstitute.WhoRelatedToInstituteService;
import com.myproject.myworkshop.service.workshop.OfferedWorkShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/institute")
public class InstituteController {

    @Autowired
    private InstituteService instituteService;

    @Autowired
    private UserService userService;

    @Autowired
    private myGroupService mygroupservice;


    @Autowired
    private RoleService roleService;


    @Autowired
    private WhoRelatedToInstituteService whoRelatedToInstituteService;


    @Autowired
    private WorkShopService workShopService;

    @Autowired
    private OfferedWorkShopService offeredWorkShopService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private OrganizerWorkShopRelationService organizerWorkShopRelationService;


    @Autowired
    private GraderGroupRelationService graderGroupRelationService;

    @Autowired
    private InstituteWhoRelatedToWorkShopService instituteWhoRelatedToWorkShopService;

    @GetMapping (value = "/first")
    public ResponseEntity saveinitialinstitute() {

        Institute institute = new Institute();
        Date nowDate = new Date(System.currentTimeMillis());
        List<WorkShop> workShops = new ArrayList<>();

        List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelations = new ArrayList<>();
        List<Role> Roles = new ArrayList<>();
        InstituteWhoRelatedToWorkShopRelation instituteWhoRelatedToWorkShopRelation = new InstituteWhoRelatedToWorkShopRelation();
        WhoRelatedToInstitute whoRelatedToInstitute = new WhoRelatedToInstitute();
        instituteWhoRelatedToWorkShopRelation.setAccountantOrManager(AccountantOrManager.manager);

        Role r = roleService.save(whoRelatedToInstitute);
        whoRelatedToInstituteService.save(whoRelatedToInstitute);

        instituteService.save(institute);
        instituteWhoRelatedToWorkShopRelation.setWhoRelatedToInstitute((WhoRelatedToInstitute) r);
        instituteWhoRelatedToWorkShopRelations.add(instituteWhoRelatedToWorkShopRelation);
        instituteWhoRelatedToWorkShopService.save(instituteWhoRelatedToWorkShopRelation);
        ((WhoRelatedToInstitute) r).setInstituteWhoRelatedToWorkShopRelation(instituteWhoRelatedToWorkShopRelations);
        instituteWhoRelatedToWorkShopRelation.setInstitute(institute);
        institute.setInstituteWhoRelatedToWorkShopRelation(instituteWhoRelatedToWorkShopRelations);
        Roles.add((WhoRelatedToInstitute) r);
        ((WhoRelatedToInstitute) r).setStrartdate(nowDate);

        myUser myuser = new myUser();
        myuser = userService.systemManager();
        myuser.setRoles(Roles);
        myuser.setGender(Gender.female);
        instituteService.save(institute);

        r.setUser(myuser);
        r = roleService.save(whoRelatedToInstitute);

        WorkShop workShop1 = new WorkShop();
        workShop1.setName("DeepLearning");
        workShop1.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");


        WorkShop workShop2 = new WorkShop();
        workShop2.setName("IBM DATASCIENCE");
        workShop2.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");

        WorkShop workShop3 = new WorkShop();
        workShop3.setName("DataScience");
        workShop3.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");

        WorkShop workShop4 = new WorkShop();
        workShop4.setName("Introduction To Data Science");
        workShop4.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");

        workShops.add(workShop1);
        workShops.add(workShop2);
        workShops.add(workShop3);
        workShops.add(workShop4);

        workShopService.save(workShop1);
        workShopService.save(workShop2);
        workShopService.save(workShop3);
        workShopService.save(workShop4);

        OfferedWorkShop offeredWorkShop = new OfferedWorkShop();
        List<OfferedWorkShop> offeredWorkShops = new ArrayList<>();
        offeredWorkShops.add(offeredWorkShop);

        workShop1.setOfferedWorkShops(offeredWorkShops);
        offeredWorkShop.setWorkShop(workShop1);

        myUser mygrader = new myUser();
        myUser mypartcipant = new myUser();
        myUser myorganizer = new myUser();

        mygrader.setUserName("Grader");
        mypartcipant.setUserName("Partcipant");
        myorganizer.setUserName("Organizer");

        mygrader.setPassword(bCryptPasswordEncoder.encode("12345"));
        mypartcipant.setPassword(bCryptPasswordEncoder.encode("123456"));
        myorganizer.setPassword(bCryptPasswordEncoder.encode("1234567"));

        List<Role> mygraderroles = new ArrayList<>();
        List<Role> myparticipantroles = new ArrayList<>();
        List<Role> myorganizerroles = new ArrayList<>();

        List<OrganizerWorkshopRelation> organizerWorkshopRelations= new ArrayList<>();
        List<GraderGroupRelation> graderGroupRelations= new ArrayList<>();
        List<ParticipantGroupRelation> participantGroupRelations= new ArrayList<>();

        Organizer organizer = new Organizer();
        Grader grader = new Grader();
        Participant participant = new Participant();

        mygraderroles.add(grader);
        myorganizerroles.add(organizer);
        myparticipantroles.add(participant);

        OrganizerWorkshopRelation organizerWorkshopRelation = new OrganizerWorkshopRelation();
        GraderGroupRelation graderGroupRelation = new GraderGroupRelation();
        ParticipantGroupRelation participantGroupRelation = new ParticipantGroupRelation();

        organizerWorkshopRelations.add(organizerWorkshopRelation);
        graderGroupRelations.add(graderGroupRelation);
        participantGroupRelations.add(participantGroupRelation);

        myGroup mygroupparticipant = new myGroup();
        myGroup mygroupgrader = new myGroup();

        List<myGroup> myGroups = new ArrayList<>();
        myGroups.add(mygroupgrader);


        mygrader.setRoles(mygraderroles);
        grader.setUser(mygrader);
        userService.save(mygrader);
        grader.setGraderGroupRelation(graderGroupRelations);
        roleService.save(grader);
        graderGroupRelation.setGrader(grader);
        roleService.save(grader);
        graderGroupRelation.setMygroup(mygroupgrader);
        mygroupgrader.setGraderGroupRelation(graderGroupRelations);
        mygroupgrader.setOfferedWorkShop(offeredWorkShop);
        offeredWorkShop.setMygroup(myGroups);

//        mygroupservice.save(mygroupgrader);
//        graderGroupRelationService.save(graderGroupRelation);

        mypartcipant.setRoles(myparticipantroles);
        participant.setUser(mypartcipant);
        userService.save(mypartcipant);
        participant.setParticipantGroupRelation(participantGroupRelations);
        roleService.save(participant);
        participantGroupRelation.setParticipant(participant);
        roleService.save(participant);
        participantGroupRelation.setMygroup(mygroupparticipant);
        mygroupparticipant.setParticipantGroupRelations(participantGroupRelations);
        mygroupparticipant.setOfferedWorkShop(offeredWorkShop);
        offeredWorkShop.setMygroup(myGroups);

//        mygroupservice.save(mygroupparticipant);
//        participantGroupRelationService.save(participantGroupRelation);


        myorganizer.setRoles(myorganizerroles);
        organizer.setUser(myorganizer);
        userService.save(myorganizer);
        organizer.setOrganizerWorkShopRelation(organizerWorkshopRelations);
        organizerWorkshopRelation.setOrganizer(organizer);
        roleService.save(organizer);
        organizerWorkshopRelation.setOfferedWorkshop(offeredWorkShop);
        offeredWorkShop.setOrganizerWorkshopRelation(organizerWorkshopRelations);
        offeredWorkShopService.save(offeredWorkShop);
        organizerWorkShopRelationService.save(organizerWorkshopRelation);

        return new ResponseEntity(myuser, HttpStatus.OK);

    }

}
