package com.myproject.myworkshop.controller;


import com.myproject.myworkshop.model.institute.*;
import com.myproject.myworkshop.model.alaki.WorkShopANDOfferedWorkShop;
import com.myproject.myworkshop.model.user.Role;
import com.myproject.myworkshop.model.user.instituterelated.AccountantOrManager;
import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.*;
import com.myproject.myworkshop.repository.OfferedWorkShopRepository;
import com.myproject.myworkshop.repository.OrganizerWorkShopRelationRepository;
import com.myproject.myworkshop.repository.ScheduleRepository;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.institutewhorelatedtoworkshop.InstituteWhoRelatedToWorkShopService;
import com.myproject.myworkshop.service.myworkshop.WorkShopService;
import com.myproject.myworkshop.service.organizer.OrganizerService;
import com.myproject.myworkshop.service.organizerworkshoprelation.OrganizerWorkShopRelationService;
import com.myproject.myworkshop.service.role.RoleService;
import com.myproject.myworkshop.service.schedule.ScheduleService;
import com.myproject.myworkshop.service.user.UserService;
import com.myproject.myworkshop.service.workshop.OfferedWorkShopService;
import com.myproject.myworkshop.service.workshopworkshoprelation.WorkShopWorkShopRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/OfferedWorkShop")
public class OfferedWorkShopController {

    @Autowired
    private OfferedWorkShopService offeredWorkShopService;

    @Autowired
    private UserService userService;

    @Autowired
    private WorkShopService workShopService;

    @Autowired
    private InstituteService instituteService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private WorkShopWorkShopRelationService workShopWorkShopRelationService;

    @Autowired
    private OrganizerService organizerService;

    @Autowired
    private ScheduleService scheduleService;


    @Autowired
    private OrganizerWorkShopRelationService organizerWorkShopRelationService;


    @Autowired
    private InstituteWhoRelatedToWorkShopService instituteWhoRelatedToWorkShopService;

    @GetMapping(value = "/show/workshop", produces = "application/json")
    public ResponseEntity showWorkShops() {

        return new ResponseEntity(workShopService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/show/someworkshop", produces = "application/json")
    public ResponseEntity someshowWorkShops() {
        int i = 6;
        List<WorkShop> workShops = new ArrayList<>();
        for(WorkShop workShop:workShopService.findAll()){
            if(i<0)
                break;
            workShops.add(workShop);
        }
        return new ResponseEntity(workShops, HttpStatus.OK);
    }

    @GetMapping(value = "/fake", produces = "application/json")
    public ResponseEntity addfakeWorkShops() {

        WorkShop workShop1 = new WorkShop();
        workShop1.setName("DeepLearning");
        workShop1.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");


        WorkShop workShop2 = new WorkShop();
        workShop2.setName("IBM DATASCIENCE");
        workShop2.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");

        WorkShop workShop3 = new WorkShop();
        workShop3.setName("DataScience");
        workShop3.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");

        WorkShop workShop4 = new WorkShop();
        workShop4.setName("Introduction To Data Science");
        workShop4.setDescription("This course offers lecture, laboratory, and online interaction to provide a foundation in data managementconcepts and database systems");

        workShopService.save(workShop1);
        workShopService.save(workShop2);
        workShopService.save(workShop3);
        workShopService.save(workShop4);

        return new ResponseEntity(workShop2, HttpStatus.OK);
    }

    @GetMapping(value = "/show/theOrganizer", produces = "application/json")
    public ResponseEntity showtheOrganizers(){

        return new ResponseEntity(organizerService.findAll(), HttpStatus.OK);
    }

    @PostMapping(value = "/add", produces = "application/json")
    public ResponseEntity addOfferedWorkShop(@RequestHeader(value = "Authorization") String token, @RequestBody WorkShopANDOfferedWorkShop workshopandofferedWorkShop) {
        Institute myinstitute = new Institute();

        myUser myuser = userService.findByUserName(workshopandofferedWorkShop.getOrganizer().getUserName());

        if (userService.findByUserName(workshopandofferedWorkShop.getOrganizer().getUserName()) != null) {
            List<Role> roles = new ArrayList<>();
            roles = myuser.getRoles();
            Organizer organizer = new Organizer();
            OrganizerWorkshopRelation organizerWorkShopRelation = new OrganizerWorkshopRelation();
            List<OrganizerWorkshopRelation> organizerWorkshopRelations = new ArrayList<>();
            organizerWorkshopRelations.add(organizerWorkShopRelation);
            organizer.setOrganizerWorkShopRelation(organizerWorkshopRelations);
            organizerWorkShopRelation.setOrganizer(organizer);
            organizerService.save(organizer);

            roles.add(organizer);
            myuser.setRoles(roles);
            organizer.setUser(myuser);
        }

        for (Institute institute : instituteService.findAll()) {
            myinstitute = institute;
        }
        List<Schedule> schedules = new ArrayList<>();
        List<WorkShop> workShops = new ArrayList<>();
        workShops = myinstitute.getWorkShops();
        WorkShop workShop = new WorkShop();
        OfferedWorkShop offeredWorkShop = new OfferedWorkShop();
        offeredWorkShop = workshopandofferedWorkShop.getOfferedWorkShop();
        List<OfferedWorkShop> offeredWorkShops= new ArrayList<OfferedWorkShop>();
        offeredWorkShops.add(offeredWorkShop);
        workShop = workshopandofferedWorkShop.getWorkShop();
        workShop.setInstitute(myinstitute);
        workShop.setOfferedWorkShops(offeredWorkShops);
        offeredWorkShop.setWorkShop(workShop);

        return new ResponseEntity(workshopandofferedWorkShop.getOfferedWorkShop(), HttpStatus.OK);

    }


    @GetMapping(value = "/show/organizer", produces = "application/json")
    public ResponseEntity showWorkShopOrganizer(@RequestHeader(value = "Authorization") String token) {
        myUser myuser = new myUser();
        List<WorkShop> workShops = new ArrayList<>();
        for (WorkShop workShop : workShopService.findAll()) {
            for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                for (OrganizerWorkshopRelation organizerWorkshopRelation : offeredWorkShop.getOrganizerWorkshopRelation()) {
                    if (organizerWorkshopRelation.getOrganizer().getUser().equals(myuser)) {
                        workShops.add(workShop);
                    }
                }
            }
        }

        return new ResponseEntity(workShops, HttpStatus.OK);
    }


    @GetMapping(value = "/show/grader", produces = "application/json")
    public ResponseEntity showWorkShopGrader(@RequestHeader(value = "Authorization") String token) {
        myUser myuser = new myUser();
        List<WorkShop> workShops = new ArrayList<>();
        for (WorkShop workShop : workShopService.findAll()) {
            for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                for (myGroup mygroup : offeredWorkShop.getMygroup()) {
                    for (GraderGroupRelation gradergroupRelation : mygroup.getGraderGroupRelation())
                        if (gradergroupRelation.getGrader().getUser().equals(myuser)) {
                            workShops.add(workShop);
                        }
                }
            }
        }

        return new ResponseEntity(workShops, HttpStatus.OK);
    }


    @GetMapping(value = "/show/participant", produces = "application/json")
    public ResponseEntity showWorkShopParticipant(@RequestHeader(value = "Authorization") String token) {
        myUser myuser = new myUser();
        List<WorkShop> workShops = new ArrayList<>();
        for (WorkShop workShop : workShopService.findAll()) {
            for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                for (myGroup mygroup : offeredWorkShop.getMygroup()) {
                    for (ParticipantGroupRelation participantGroupRelation : mygroup.getParticipantGroupRelations())
                        if (participantGroupRelation.getParticipant().getUser().equals(myuser)) {
                            workShops.add(workShop);
                        }
                }
            }
        }

        return new ResponseEntity(workShops, HttpStatus.OK);
    }


    @PostMapping(value = "/search", produces = "application/json")
    public ResponseEntity searchWorkShop(@RequestHeader(value = "Authorization") String token, @RequestBody String subword) {

        List<OfferedWorkShop> offeredWorkShops = new ArrayList<>();
        for (WorkShop workShop : workShopService.findAll()) {
            if (workShop.getName().contains(subword))
                for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                    offeredWorkShops.add(offeredWorkShop);
                }
        }

        return new ResponseEntity(offeredWorkShops, HttpStatus.OK);
    }

    @PostMapping(value = "/isOrganizer", produces = "application/json")
    public ResponseEntity isOrganizer(@RequestHeader(value = "Authorization", required = false) String token) {

        boolean isOKay = false;
        for (WorkShop workShop : workShopService.findAll()) {
            for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                if (userService.isOrganizer(token, offeredWorkShop)) {
                    isOKay = true;
                }
            }
        }

        return new ResponseEntity(isOKay, HttpStatus.OK);

    }

    @PostMapping(value = "/isAdmin", produces = "application/json")
    public ResponseEntity isAdmin(@RequestHeader(value = "Authorization", required = false) String token) {
        System.out.println("hello");
        boolean isOKay = false;
        if (userService.isManager(token))
            isOKay = true;

        return new ResponseEntity(isOKay, HttpStatus.OK);

    }


    @PostMapping(value = "/isGrader", produces = "application/json")
    public ResponseEntity isGrader(@RequestHeader(value = "Authorization") String token) {

        boolean isOKay = false;
        for (WorkShop workShop : workShopService.findAll()) {
            for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                if (userService.isGrader(token, offeredWorkShop)) {
                    isOKay = true;
                }
            }
        }

        return new ResponseEntity(isOKay, HttpStatus.OK);

    }

    @PostMapping(value = "/isParticipant", produces = "application/json")
    public ResponseEntity isParticipant(@RequestHeader(value = "Authorization") String token) {

        boolean isOKay = false;
        for (WorkShop workShop : workShopService.findAll()) {
            for (OfferedWorkShop offeredWorkShop : workShop.getOfferedWorkShops()) {
                if (userService.isParticipant(token, offeredWorkShop)) {
                    isOKay = true;
                }
            }
        }

        return new ResponseEntity(isOKay, HttpStatus.OK);

    }
}
