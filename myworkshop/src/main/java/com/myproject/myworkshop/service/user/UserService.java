package com.myproject.myworkshop.service.user;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.model.user.LoginUser;
import com.myproject.myworkshop.model.user.Token;
import com.myproject.myworkshop.model.user.myUser;
import sun.util.locale.StringTokenIterator;

import java.util.Optional;


public interface UserService {

   myUser findByUserName(String username);

   void save(myUser user);

   myUser systemManager();

    Optional<myUser> findById(Integer id);

    Iterable<myUser> findAll();

    Token login(LoginUser loginUser);

    void signup(myUser user);

    boolean hasBeenOrganizer(myUser myuser);

    boolean isManager(String Token);

    boolean isGrader(String Token, OfferedWorkShop offeredWorkShop);

    boolean isParticipant(String Token,OfferedWorkShop offeredWorkShop);

    boolean isOrganizer(String Token,OfferedWorkShop offeredWorkShop);

    myUser findUser(String token);

    boolean isGraderByUser(myUser myuser,OfferedWorkShop offeredWorkShop);

    boolean isParticipantByUser(myUser myuser,OfferedWorkShop offeredWorkShop);

}
