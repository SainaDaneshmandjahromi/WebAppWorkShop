package com.myproject.myworkshop.service.organizer.impl;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.model.user.workshoprelated.Organizer;
import com.myproject.myworkshop.repository.InstituteRepository;
import com.myproject.myworkshop.repository.OrganizerRepository;
import com.myproject.myworkshop.repository.WorkShopRepository;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.myworkshop.WorkShopService;
import com.myproject.myworkshop.service.organizer.OrganizerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizerServiceImpl implements OrganizerService {
    @Autowired
    private OrganizerRepository organizerRepository;
    @Override
    public void save(Organizer organizer) { organizerRepository.save(organizer);
    }

    @Override
    public Iterable<Organizer> findAll() {
        return organizerRepository.findAll();
    }


}
