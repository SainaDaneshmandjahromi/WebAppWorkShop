package com.myproject.myworkshop.service.myworkshop;

import com.myproject.myworkshop.model.institute.WorkShop;

public interface WorkShopService {
    void save(WorkShop workShop);

    WorkShop findByName(String name);

    Iterable<WorkShop> findAll();

}
