package com.myproject.myworkshop.service.role.impl;

import com.myproject.myworkshop.model.user.Role;
import com.myproject.myworkshop.repository.RoleRepository;
import com.myproject.myworkshop.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Iterable<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }
}
