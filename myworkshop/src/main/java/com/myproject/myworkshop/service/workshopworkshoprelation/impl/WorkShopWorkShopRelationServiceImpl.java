package com.myproject.myworkshop.service.workshopworkshoprelation.impl;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.WorkShopWorkShopRelation;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.repository.OfferedWorkShopRepository;
import com.myproject.myworkshop.repository.UserRepository;
import com.myproject.myworkshop.repository.WorkShopWorkShopRelationRepository;
import com.myproject.myworkshop.service.workshopworkshoprelation.WorkShopWorkShopRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class WorkShopWorkShopRelationServiceImpl implements WorkShopWorkShopRelationService {

    @Autowired
    private WorkShopWorkShopRelationRepository workShopWorkShopRelationRepository;

    @Override
    public Iterable<WorkShopWorkShopRelation> findAll() {
        return workShopWorkShopRelationRepository.findAll();
    }


    @Override
    public void save(WorkShopWorkShopRelation workShopWorkShopRelation) {
        workShopWorkShopRelationRepository.save(workShopWorkShopRelation);
    }

}
