package com.myproject.myworkshop.service.questionanswer.impl;
import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.repository.QuestionAnswerRepository;
import com.myproject.myworkshop.service.questionanswer.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionAnswerServiceImpl implements QuestionAnswerService {

    @Autowired
    private QuestionAnswerRepository questionAnswerRepository;

    @Override
    public void save(QuestionAnswer questionAnswer) { questionAnswerRepository.save(questionAnswer);
    }


//    @Override
//    public void update(QuestionAnswer questionAnswer) { questionAnswerRepository.update(questionAnswer);
//    }
   }

