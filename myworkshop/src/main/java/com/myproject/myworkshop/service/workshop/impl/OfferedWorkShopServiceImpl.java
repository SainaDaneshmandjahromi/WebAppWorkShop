package com.myproject.myworkshop.service.workshop.impl;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.repository.OfferedWorkShopRepository;
import com.myproject.myworkshop.repository.UserRepository;
import com.myproject.myworkshop.service.workshop.OfferedWorkShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OfferedWorkShopServiceImpl implements OfferedWorkShopService {

    @Autowired
    private OfferedWorkShopRepository offeredWorkShopRepository;

    @Override
    public Iterable<OfferedWorkShop> findAll() {
        return offeredWorkShopRepository.findAll();
    }


    @Override
    public void save(OfferedWorkShop offeredWorkShop) {
        offeredWorkShopRepository.save(offeredWorkShop);
    }

}
