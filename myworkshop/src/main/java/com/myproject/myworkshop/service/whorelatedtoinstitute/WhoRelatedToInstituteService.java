package com.myproject.myworkshop.service.whorelatedtoinstitute;

import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;
import com.myproject.myworkshop.model.user.myUser;

public interface WhoRelatedToInstituteService {

    void save(WhoRelatedToInstitute whoRelatedToInstitute);
}
