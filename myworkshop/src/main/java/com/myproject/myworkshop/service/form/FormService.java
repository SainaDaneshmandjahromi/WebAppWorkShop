package com.myproject.myworkshop.service.form;

import com.myproject.myworkshop.model.form.Form;
import com.myproject.myworkshop.model.institute.Institute;

public interface FormService {
    void save(Form form);
    Form findByName(String name);
}
