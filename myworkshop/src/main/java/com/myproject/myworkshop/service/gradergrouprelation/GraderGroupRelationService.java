package com.myproject.myworkshop.service.gradergrouprelation;

import com.myproject.myworkshop.model.user.workshoprelated.GraderGroupRelation;

public interface GraderGroupRelationService {

    void save(GraderGroupRelation graderGroupRelation);

}
