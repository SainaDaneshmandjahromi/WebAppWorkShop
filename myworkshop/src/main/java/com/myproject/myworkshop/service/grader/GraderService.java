package com.myproject.myworkshop.service.grader;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.user.workshoprelated.Grader;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;

public interface GraderService {
    void save(Grader grader);
}

