package com.myproject.myworkshop.service.schedule;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.Schedule;
import com.myproject.myworkshop.model.user.myUser;

public interface ScheduleService {

    Iterable<Schedule> findAll();

    void save(Schedule schedule);

}
