package com.myproject.myworkshop.service.organizer;

import com.myproject.myworkshop.model.user.workshoprelated.Organizer;

public interface OrganizerService {
    void save (Organizer organizer);

    Iterable<Organizer> findAll();
}
