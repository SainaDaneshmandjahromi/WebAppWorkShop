package com.myproject.myworkshop.service.Participant;

import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.Participant;

public interface ParticipantService {

    void save(Participant participant);

}
