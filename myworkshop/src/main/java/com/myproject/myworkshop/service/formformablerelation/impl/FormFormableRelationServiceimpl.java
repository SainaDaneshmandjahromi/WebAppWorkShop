package com.myproject.myworkshop.service.formformablerelation.impl;
import com.myproject.myworkshop.model.form.FormFormableRelation;
import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.user.workshoprelated.GraderGroupRelation;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;
import com.myproject.myworkshop.repository.*;
import com.myproject.myworkshop.service.formformablerelation.FormFormableRelationService;
import com.myproject.myworkshop.service.gradergrouprelation.GraderGroupRelationService;
import com.myproject.myworkshop.service.organizerworkshoprelation.OrganizerWorkShopRelationService;
import com.myproject.myworkshop.service.participantgrouprelation.ParticipantGroupRelationService;
import com.myproject.myworkshop.service.questionanswer.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormFormableRelationServiceimpl implements FormFormableRelationService {

    @Autowired
    private FormFormableRelationRepository formFormableRelationRepository;

    @Override
    public void save(FormFormableRelation formFormableRelation) { formFormableRelationRepository.save(formFormableRelation);
    }

}

