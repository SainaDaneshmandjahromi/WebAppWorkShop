package com.myproject.myworkshop.service.institutewhorelatedtoworkshop;

import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import com.myproject.myworkshop.model.user.myUser;

public interface InstituteWhoRelatedToWorkShopService {

    void save(InstituteWhoRelatedToWorkShopRelation instituteWhoRelatedToWorkShopRelation);
}
