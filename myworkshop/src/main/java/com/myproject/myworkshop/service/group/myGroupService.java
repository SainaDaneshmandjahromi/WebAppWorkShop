package com.myproject.myworkshop.service.group;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;

public interface myGroupService {
    void save(myGroup mygroup);
    myGroup findByName(String name);
}

