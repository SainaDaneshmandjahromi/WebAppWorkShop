package com.myproject.myworkshop.service.institutewhorelatedtoworkshop.impl;
import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;
import com.myproject.myworkshop.repository.InstituteWhoRelatedToWorkShopRepository;
import com.myproject.myworkshop.repository.OrganizerWorkShopRelationRepository;
import com.myproject.myworkshop.repository.ParticipantGroupRelationRepository;
import com.myproject.myworkshop.repository.QuestionAnswerRepository;
import com.myproject.myworkshop.service.institutewhorelatedtoworkshop.InstituteWhoRelatedToWorkShopService;
import com.myproject.myworkshop.service.organizerworkshoprelation.OrganizerWorkShopRelationService;
import com.myproject.myworkshop.service.participantgrouprelation.ParticipantGroupRelationService;
import com.myproject.myworkshop.service.questionanswer.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InstituteWhoRelatedToWorkShopServiceImpl implements InstituteWhoRelatedToWorkShopService {

    @Autowired
    private InstituteWhoRelatedToWorkShopRepository instituteWhoRelatedToWorkShopRepository;

    @Override
    public void save(InstituteWhoRelatedToWorkShopRelation instituteWhoRelatedToWorkShopRelation)
    { instituteWhoRelatedToWorkShopRepository.save(instituteWhoRelatedToWorkShopRelation);
    }

}

