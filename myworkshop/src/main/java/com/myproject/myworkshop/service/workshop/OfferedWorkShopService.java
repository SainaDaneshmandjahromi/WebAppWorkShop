package com.myproject.myworkshop.service.workshop;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.user.myUser;

public interface OfferedWorkShopService {

    Iterable<OfferedWorkShop> findAll();

    void save(OfferedWorkShop offeredWorkShop);

}
