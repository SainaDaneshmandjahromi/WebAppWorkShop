package com.myproject.myworkshop.service.role;

import com.myproject.myworkshop.model.user.Role;

public interface RoleService {
    Iterable<Role> findAll();

    Role save(Role role);
}
