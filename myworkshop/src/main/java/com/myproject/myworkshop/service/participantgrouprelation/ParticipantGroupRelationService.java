package com.myproject.myworkshop.service.participantgrouprelation;

import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;

public interface ParticipantGroupRelationService {

    void save(ParticipantGroupRelation participantGroupRelation);

}
