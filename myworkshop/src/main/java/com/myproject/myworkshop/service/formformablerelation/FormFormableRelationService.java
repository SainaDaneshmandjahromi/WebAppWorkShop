package com.myproject.myworkshop.service.formformablerelation;

import com.myproject.myworkshop.model.form.FormFormableRelation;
import com.myproject.myworkshop.model.user.workshoprelated.GraderGroupRelation;

public interface FormFormableRelationService {

    void save(FormFormableRelation formFormableRelation);

}
