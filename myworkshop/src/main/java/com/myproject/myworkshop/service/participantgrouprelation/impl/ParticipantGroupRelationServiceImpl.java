package com.myproject.myworkshop.service.participantgrouprelation.impl;
import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;
import com.myproject.myworkshop.repository.ParticipantGroupRelationRepository;
import com.myproject.myworkshop.repository.QuestionAnswerRepository;
import com.myproject.myworkshop.service.participantgrouprelation.ParticipantGroupRelationService;
import com.myproject.myworkshop.service.questionanswer.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParticipantGroupRelationServiceImpl implements ParticipantGroupRelationService {

    @Autowired
    private ParticipantGroupRelationRepository participantGroupRelationRepository;

    @Override
    public void save(ParticipantGroupRelation participantGroupRelation) { participantGroupRelationRepository.save(participantGroupRelation);
    }

}

