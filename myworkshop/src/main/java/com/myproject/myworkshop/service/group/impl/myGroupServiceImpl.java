package com.myproject.myworkshop.service.group.impl;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;
import com.myproject.myworkshop.repository.myGroupRepository;
import com.myproject.myworkshop.service.group.myGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class myGroupServiceImpl implements myGroupService {

    @Autowired
    private myGroupRepository mygroupRepository;

    @Override
    public void save(myGroup mygroup) { mygroupRepository.save(mygroup);
    }

    public myGroup findByName(String name) {
        return mygroupRepository.findByName(name);
    }
}

