package com.myproject.myworkshop.service.grader.GraderServiceImpl;

import com.myproject.myworkshop.model.user.workshoprelated.Grader;
import com.myproject.myworkshop.repository.GraderRepository;
import com.myproject.myworkshop.service.grader.GraderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraderServiceImpl implements GraderService {

    @Autowired
    private GraderRepository graderRepository;

    @Override
    public void save(Grader grader) { graderRepository.save(grader);
    }

    }

