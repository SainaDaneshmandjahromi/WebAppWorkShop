package com.myproject.myworkshop.service.organizerworkshoprelation.impl;
import com.myproject.myworkshop.model.form.QuestionAnswer;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;
import com.myproject.myworkshop.repository.OrganizerWorkShopRelationRepository;
import com.myproject.myworkshop.repository.ParticipantGroupRelationRepository;
import com.myproject.myworkshop.repository.QuestionAnswerRepository;
import com.myproject.myworkshop.service.organizerworkshoprelation.OrganizerWorkShopRelationService;
import com.myproject.myworkshop.service.participantgrouprelation.ParticipantGroupRelationService;
import com.myproject.myworkshop.service.questionanswer.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizerWorkShopRelationServiceImpl implements OrganizerWorkShopRelationService {

    @Autowired
    private OrganizerWorkShopRelationRepository organizerWorkShopRelationRepository;

    @Override
    public void save(OrganizerWorkshopRelation organizerWorkShopRelation) { organizerWorkShopRelationRepository.save(organizerWorkShopRelation);
    }

}

