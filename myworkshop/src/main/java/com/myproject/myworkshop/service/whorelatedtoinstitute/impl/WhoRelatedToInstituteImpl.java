package com.myproject.myworkshop.service.whorelatedtoinstitute.impl;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;
import com.myproject.myworkshop.repository.InstituteRepository;
import com.myproject.myworkshop.repository.WhoRelatedToInstituteRepository;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.whorelatedtoinstitute.WhoRelatedToInstituteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WhoRelatedToInstituteImpl implements WhoRelatedToInstituteService {
    @Autowired
    private WhoRelatedToInstituteRepository whoRelatedToInstituteRepository;

    @Override
    public void save(WhoRelatedToInstitute whoRelatedToInstitute) { whoRelatedToInstituteRepository.save(whoRelatedToInstitute);
    }

}
