package com.myproject.myworkshop.service.organizerworkshoprelation;

import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;

public interface OrganizerWorkShopRelationService {

    void save(OrganizerWorkshopRelation organizerWorkshopRelation);

}
