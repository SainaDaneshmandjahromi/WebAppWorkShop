package com.myproject.myworkshop.service.Participant.Impl;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.Participant;
import com.myproject.myworkshop.repository.OrganizerWorkShopRelationRepository;
import com.myproject.myworkshop.repository.ParticipantRepository;
import com.myproject.myworkshop.service.Participant.ParticipantService;
import com.myproject.myworkshop.service.organizerworkshoprelation.OrganizerWorkShopRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParticipantServiceImpl implements ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public void save(Participant participant) { participantRepository.save(participant);
    }

}

