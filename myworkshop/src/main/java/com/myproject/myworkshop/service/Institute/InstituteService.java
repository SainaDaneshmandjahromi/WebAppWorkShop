package com.myproject.myworkshop.service.Institute;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.user.myUser;

public interface InstituteService {
    void save(Institute institute);
    Institute findByName(String name);
    Iterable<Institute> findAll();

}
