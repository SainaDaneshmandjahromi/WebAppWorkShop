package com.myproject.myworkshop.service.workshopworkshoprelation;

import com.myproject.myworkshop.model.institute.WorkShopWorkShopRelation;

public interface WorkShopWorkShopRelationService {

    Iterable<WorkShopWorkShopRelation> findAll();

    void save(WorkShopWorkShopRelation workShopWorkShopRelation);

}
