package com.myproject.myworkshop.service.form.Impl;

import com.myproject.myworkshop.model.form.Form;
import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.repository.FormRepository;
import com.myproject.myworkshop.repository.InstituteRepository;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.form.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormServiceImpl implements FormService {
    @Autowired
    private FormRepository formRepository;
    @Override
    public void save(Form form) { formRepository.save(form);
    }
    @Override
    public Form findByName(String name) {

        return formRepository.findByName(name);
    }
}
