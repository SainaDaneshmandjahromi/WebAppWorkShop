package com.myproject.myworkshop.service.myworkshop.impl;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.repository.InstituteRepository;
import com.myproject.myworkshop.repository.WorkShopRepository;
import com.myproject.myworkshop.service.Institute.InstituteService;
import com.myproject.myworkshop.service.myworkshop.WorkShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkShopServiceImpl implements WorkShopService {
    @Autowired
    private WorkShopRepository workShopRepository;
    @Override
    public void save(WorkShop workShop) { workShopRepository.save(workShop);
    }
    @Override
    public WorkShop findByName(String name) {

        return workShopRepository.findByName(name);
    }

    @Override
    public Iterable<WorkShop> findAll() {
        return workShopRepository.findAll();
    }


}
