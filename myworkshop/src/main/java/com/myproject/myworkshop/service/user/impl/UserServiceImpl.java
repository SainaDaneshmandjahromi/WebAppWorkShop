package com.myproject.myworkshop.service.user.impl;

import com.myproject.myworkshop.model.institute.InstituteWhoRelatedToWorkShopRelation;
import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.user.*;
import com.myproject.myworkshop.model.user.instituterelated.AccountantOrManager;
import com.myproject.myworkshop.model.user.instituterelated.WhoRelatedToInstitute;
import com.myproject.myworkshop.model.user.workshoprelated.GraderGroupRelation;
import com.myproject.myworkshop.model.user.workshoprelated.OrganizerWorkshopRelation;
import com.myproject.myworkshop.model.user.workshoprelated.ParticipantGroupRelation;
import com.myproject.myworkshop.model.user.workshoprelated.myGroup;
import com.myproject.myworkshop.repository.InstituteRepository;
import com.myproject.myworkshop.repository.OfferedWorkShopRepository;
import com.myproject.myworkshop.repository.UserRepository;
import com.myproject.myworkshop.service.user.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.myproject.myworkshop.security.SecurityConstants.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InstituteRepository instituteRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private OfferedWorkShopRepository offeredWorkShopRepository;

    public myUser findByUserName(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public Iterable<myUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void save(myUser user) {
        userRepository.save(user);
    }

    @Override
    public void signup(myUser user) {
    }

    public Optional<myUser> findById(Integer id) {
        return userRepository.findById(id);
    }

    public myUser systemManager() {
        myUser myuser = new myUser();
        myuser.setName("Admin");
        myuser.setUserName("Adminuser");
        myuser.setPassword(bCryptPasswordEncoder.encode("1234"));
        userRepository.save(myuser);

        return myuser;
    }

    public boolean isManager(String token) {
        Token mytoken = new Token(token);
        List<Role> myRoles = new ArrayList<Role>();
        List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelations = new ArrayList<>();
        Map m = (HashMap) (mytoken.decodeJWT().get("principal"));
        if (userRepository.findById(Integer.parseInt((String) m.get("id"))).isPresent()) {
            myUser u = userRepository.findById(Integer.parseInt((String) m.get("id"))).get();
            myRoles = u.getRoles();

            for (Role role : myRoles) {
                if (role instanceof WhoRelatedToInstitute) {
                    instituteWhoRelatedToWorkShopRelations =
                            (((WhoRelatedToInstitute) role).getInstituteWhoRelatedToWorkShopRelation());
                    for (InstituteWhoRelatedToWorkShopRelation instituteWhoRelatedToWorkShopRelation : instituteWhoRelatedToWorkShopRelations) {
                        if (instituteWhoRelatedToWorkShopRelation.getAccountantOrManager().equals(AccountantOrManager.manager)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isAccountant(String token) {
        Token mytoken = new Token(token);
        List<Role> myRoles = new ArrayList<Role>();
        List<InstituteWhoRelatedToWorkShopRelation> instituteWhoRelatedToWorkShopRelations = new ArrayList<>();
        Map m = (HashMap) (mytoken.decodeJWT().get("principal"));
        if (userRepository.findById(Integer.parseInt((String) m.get("id"))).isPresent()) {
            myUser u = userRepository.findById(Integer.parseInt((String) m.get("id"))).get();
            myRoles = u.getRoles();

            for (Role role : myRoles) {
                if (role instanceof WhoRelatedToInstitute) {
                    instituteWhoRelatedToWorkShopRelations =
                            (((WhoRelatedToInstitute) role).getInstituteWhoRelatedToWorkShopRelation());
                    for (InstituteWhoRelatedToWorkShopRelation instituteWhoRelatedToWorkShopRelation : instituteWhoRelatedToWorkShopRelations) {
                        if (instituteWhoRelatedToWorkShopRelation.getAccountantOrManager().equals(AccountantOrManager.accountant)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isGrader(String token, OfferedWorkShop offeredWorkShopWorkShop) {
        Token mytoken = new Token(token);
        myUser myuser = new myUser();
        List<myGroup> groups = new ArrayList<>();
        Map m = (HashMap) (mytoken.decodeJWT().get("principal"));
        if (userRepository.findById(Integer.parseInt((String) m.get("id"))).isPresent()) {
            myuser = userRepository.findById(Integer.parseInt((String) m.get("id"))).get();
            groups = offeredWorkShopWorkShop.getMygroup();

            for (myGroup mygroup : groups) {
                if (mygroup.isBranch()) {
                    List<GraderGroupRelation> graderGroupRelations = new ArrayList<>();
                    graderGroupRelations = mygroup.getGraderGroupRelation();
                    for (GraderGroupRelation graderGroupRelation : graderGroupRelations) {
                        if (graderGroupRelation.getGrader().getUser().equals(myuser))
                            return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isParticipant(String token, OfferedWorkShop offeredWorkShop) {
        Token mytoken = new Token(token);
        myUser myuser = new myUser();
        List<myGroup> groups = new ArrayList<>();
        Map m = (HashMap) (mytoken.decodeJWT().get("principal"));
        if (userRepository.findById(Integer.parseInt((String) m.get("id"))).isPresent()) {
            myuser = userRepository.findById(Integer.parseInt((String) m.get("id"))).get();
            groups = offeredWorkShop.getMygroup();

            for (myGroup mygroup : groups) {
                if (mygroup.isBranch()) {
                    List<ParticipantGroupRelation> participantGroupRelations = new ArrayList<>();
                    participantGroupRelations = mygroup.getParticipantGroupRelations();
                    for (ParticipantGroupRelation participantGroupRelation : participantGroupRelations) {
                        if (participantGroupRelation.getParticipant().getUser().equals(myuser))
                            return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isOrganizer(String token, OfferedWorkShop offeredWorkShop) {
        List<myGroup> groups = new ArrayList<>();
        Token mytoken = new Token(token);
        myUser myuser = new myUser();
        List<OrganizerWorkshopRelation> organizerWorkshopRelations = new ArrayList<>();
        Map m = (HashMap) (mytoken.decodeJWT().get("principal"));
        if (userRepository.findById(Integer.parseInt((String) m.get("id"))).isPresent()) {
            myuser = userRepository.findById(Integer.parseInt((String) m.get("id"))).get();
            organizerWorkshopRelations = offeredWorkShop.getOrganizerWorkshopRelation();
            for (OrganizerWorkshopRelation organizerWorkshopRelation : organizerWorkshopRelations) {
                if(organizerWorkshopRelation.getOrganizer().getUser().equals(myuser))
                    return false;
            }
        }

        return true;
    }

    public boolean isParticipantByUser(myUser myuser, OfferedWorkShop offeredWorkShop) {
        List<myGroup> groups = new ArrayList<>();
        groups = offeredWorkShop.getMygroup();
        for (myGroup mygroup : groups) {
            List<ParticipantGroupRelation> participantGroupRelations = new ArrayList<>();
            participantGroupRelations = mygroup.getParticipantGroupRelations();
            for (ParticipantGroupRelation participantGroupRelation : participantGroupRelations) {
                if (participantGroupRelation.getParticipant().getUser().equals(myuser)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isGraderByUser(myUser myuser, OfferedWorkShop offeredWorkShop) {
        List<myGroup> groups = new ArrayList<>();
        groups = offeredWorkShop.getMygroup();

        for (myGroup mygroup : groups) {
            List<GraderGroupRelation> graderGroupRelations = new ArrayList<>();
            graderGroupRelations = mygroup.getGraderGroupRelation();
            for (GraderGroupRelation graderGroupRelation : graderGroupRelations) {
                if (graderGroupRelation.getGrader().getUser().equals(myuser)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean hasBeenOrganizer(myUser myuser) {
        for (OfferedWorkShop offeredWorkShop : offeredWorkShopRepository.findAll()) {
            for (OrganizerWorkshopRelation organizerWorkshopRelation : offeredWorkShop.getOrganizerWorkshopRelation()) {
                if (organizerWorkshopRelation.getOrganizer().getUser().equals(myuser))
                    return true;
            }
        }
        return false;
    }

    public myUser findUser(String token) {
        Token mytoken = new Token(token);
        myUser myuser = new myUser();
        Map m = (HashMap) (mytoken.decodeJWT().get("principal"));
        if (userRepository.findById(Integer.parseInt((String) m.get("id"))).isPresent()) {
            myuser = userRepository.findById(Integer.parseInt((String) m.get("id"))).get();

        }
        return myuser;
    }

    public Token login(LoginUser loginUser) {
        myUser user = findByUserName(loginUser.getUserName());

        if (user == null) {
            throw new ResourceNotFoundException("user not exists");
        }

        if (!bCryptPasswordEncoder.matches(loginUser.getPassword(), user.getPassword())) {
            throw new ResourceNotFoundException("password not correct");
        }

        LoginPrinciple loginPrincipal = new LoginPrinciple(user.getId().toString());
        Map<String, Object> claims = new HashMap<>();
        claims.put("principal", loginPrincipal);
        String token = Jwts.builder()
                .setSubject(user.getUserName())
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY.getBytes())
                .compact();

        return new Token(TOKEN_PREFIX + " " + token);
    }
}

