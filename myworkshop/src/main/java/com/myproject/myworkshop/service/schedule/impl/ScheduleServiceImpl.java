package com.myproject.myworkshop.service.schedule.impl;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;
import com.myproject.myworkshop.model.institute.Schedule;
import com.myproject.myworkshop.model.user.myUser;
import com.myproject.myworkshop.repository.OfferedWorkShopRepository;
import com.myproject.myworkshop.repository.ScheduleRepository;
import com.myproject.myworkshop.repository.UserRepository;
import com.myproject.myworkshop.service.schedule.ScheduleService;
import com.myproject.myworkshop.service.workshop.OfferedWorkShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public Iterable<Schedule> findAll() {
        return scheduleRepository.findAll();
    }


    @Override
    public void save(Schedule schedule) {
        scheduleRepository.save(schedule);
    }

}
