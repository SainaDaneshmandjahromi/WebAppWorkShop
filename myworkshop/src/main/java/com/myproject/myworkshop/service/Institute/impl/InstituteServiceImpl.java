package com.myproject.myworkshop.service.Institute.impl;

import com.myproject.myworkshop.model.institute.Institute;
import com.myproject.myworkshop.model.institute.WorkShop;
import com.myproject.myworkshop.repository.InstituteRepository;
import com.myproject.myworkshop.service.Institute.InstituteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InstituteServiceImpl implements InstituteService {
    @Autowired
    private InstituteRepository instituteRepository;
    @Override
    public void save(Institute institute) { instituteRepository.save(institute);
    }
    @Override
    public Institute findByName(String name) {

        return instituteRepository.findByName(name);
    }

    @Override
    public Iterable<Institute> findAll() {
        return instituteRepository.findAll();
    }

}
