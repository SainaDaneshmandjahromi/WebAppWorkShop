package com.myproject.myworkshop.security;

import com.myproject.myworkshop.model.institute.OfferedWorkShop;

public class SecurityConstants {
    public static final String SECRET_KEY = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/user/register";
    public static final String SIGN_IN_URL = "/api/user/login";
    public static final String TOKEN_URL = "/api/user/Token";
    public static final String SHOW_WORKSHOP_GRADER_URL = "api/OfferedWorkShop/show/grader";
    public static final String SHOW_WORKSHOP_PARTICIPANT_URL = "api/OfferedWorkShop/show/participant";
    public static final String SHOW_WORKSHOP_ORGANIZER_URL = "api/OfferedWorkShop/show/organizer";
    public static final String CREATE_GROUP_URL = "api/group/create";
    public static final String INITIAL_INSTITUTE_URL = "/api/institute/first";
    public static final String ADD_WORKSHOP_URL = "/api/OfferedWorkShop/add";
    public static final String SEARCH_URL = "/api/OfferedWorkShop/search";
    public static final String ISADMIN_URL = "/api/OfferedWorkShop/isAdmin";
    public static final String IS_PARTICIPANT_URL = "/api/OfferedWorkShop/isParticipant";
    public static final String SHOW_WORKSHOP_URL = "/api/OfferedWorkShop/show/workshop";
    public static final String SHOW_SOMEWORKSHOP_URL = "/api/OfferedWorkShop/show/someworkshop";
    public static final String IS_ORGANIZER_URL = "/api/OfferedWorkShop/isOrganizer";
    public static final String IS_GRADER_URL = "/api/OfferedWorkShop/isGrader";
    public static final String CREATE_FORM_URL = "/api/form/create";
    public static final String FAKE_URL = "/api/OfferedWorkShop/fake";
    public static final String SHOWORGANIZERS_URL = "/api/OfferedWorkShop/show/theOrganizer";

}
