package com.myproject.myworkshop.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static com.myproject.myworkshop.security.SecurityConstants.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {
    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    // define which methods are public and everything else are private
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.headers().frameOptions().disable().and()
                .cors().and().csrf().disable().authorizeRequests()
                .antMatchers(
                        "/v2/api-docs",
                        "/swagger-resources/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        /*Probably not needed*/ "/swagger.json")
                .permitAll()
//                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
                .antMatchers(HttpMethod.POST, SIGN_IN_URL).permitAll()
                .antMatchers(HttpMethod.POST, TOKEN_URL).permitAll()
                .antMatchers(HttpMethod.POST, ADD_WORKSHOP_URL).permitAll()
                .antMatchers(HttpMethod.GET, INITIAL_INSTITUTE_URL).permitAll()
                .antMatchers(HttpMethod.GET, SHOW_WORKSHOP_GRADER_URL).permitAll()
                .antMatchers(HttpMethod.GET, SHOW_WORKSHOP_PARTICIPANT_URL).permitAll()
                .antMatchers(HttpMethod.GET, SHOW_WORKSHOP_ORGANIZER_URL).permitAll()
                .antMatchers(HttpMethod.GET, SHOW_SOMEWORKSHOP_URL).permitAll()
                .antMatchers(HttpMethod.POST, SEARCH_URL).permitAll()
                .antMatchers(HttpMethod.POST, ISADMIN_URL).permitAll()
                .antMatchers(HttpMethod.POST, CREATE_GROUP_URL).permitAll()
                .antMatchers(HttpMethod.GET, SHOW_WORKSHOP_URL).permitAll()
                .antMatchers(HttpMethod.POST, IS_ORGANIZER_URL).permitAll()
                .antMatchers(HttpMethod.POST, IS_GRADER_URL).permitAll()
                .antMatchers(HttpMethod.POST, IS_PARTICIPANT_URL).permitAll()
                .antMatchers(HttpMethod.POST, CREATE_FORM_URL).permitAll()
                .antMatchers(HttpMethod.GET, SHOWORGANIZERS_URL).permitAll()
                .antMatchers(HttpMethod.GET, FAKE_URL).permitAll()
                .antMatchers(HttpMethod.GET, "/user/list").permitAll()
                .antMatchers("/", "/static/**", "/resources/**", "/resources/static/**").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/vendor/**").permitAll()
                .antMatchers("/**.js").permitAll()
                .antMatchers("/**.css").permitAll()
                .antMatchers("/**.woff2").permitAll()
                .antMatchers("/**.woff").permitAll()
                .antMatchers("/**.ttf").permitAll()
                .antMatchers("/**.html").permitAll()
                .antMatchers("/**.ico").permitAll()
                .antMatchers("/**.svg").permitAll()
                .antMatchers("/statics/time-stamp-formats").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(ImmutableList.of("*"));
//        configuration.setAllowedMethods(ImmutableList.of("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
//        configuration.setAllowedHeaders(ImmutableList.of("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
